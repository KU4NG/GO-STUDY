## 说明

GO 语言的数组我们可以把它看成 Python 中的列表，但是区别在于 GO 语言中的数组有长度和类型限制。

在 GO 语言中，数组中的元素只能是同种类型的元素，且明确元素个数。

其定义方式为：

```
var 数组名称 [数组长度]数组类型
```

数组的角标是从 `0` 开始的，这点和其它编程语言一样。





## 初始化

方法一：默认情况下，数组在没有给定初始值的时候会自动补充该类型的默认值，如 string 的 ''，bool 类型的 false。

```go
package main

import "fmt"

func main() {
	var a1 [3]int
	fmt.Println(a1)
}
```

执行结果如下：

![image-20200728164332980](img/image-20200728164332980.png)



方法二：可以在定义的同时给定数组值，当然如果给的值数量不够，会默认补充默认值。

```go
package main

import "fmt"

func main() {
	var a1 = [3]int{1, 2}
	fmt.Println(a1)
}
```

执行结果如下：

![image-20200728164553935](img/image-20200728164553935.png)



方法三：对于不知道值的数组，可以让程序去自己推导值的个数，使用 `...`。

```go
package main

import "fmt"

func main() {
	var a1 = [...]int{1, 2, 3, 4, 5}
	fmt.Println(a1)
	fmt.Println(len(a1))
}
```

执行结果如下：

![image-20200728164942566](img/image-20200728164942566.png)



方法四：某些数组只希望初始化部分数据，可以使用索引。

```go
package main

import "fmt"

func main() {
	var a1 = [5]int{1:11,3:90}
	fmt.Println(a1)
}
```

执行结果如下：

![image-20200728165156679](img/image-20200728165156679.png)



## 数组遍历

方法一：通过索引循环遍历

```go
package main

import "fmt"

func main() {
	var a1 = [4]string{"北京", "上海", "广州", "深圳"}
	for i := 0; i < len(a1); i++ {
		fmt.Println(a1[i])
	}
}
```



方法二：使用 range 遍历

```go
package main

import "fmt"

func main() {
	var a1 = [4]string{"北京", "上海", "广州", "深圳"}
	for _, v := range a1 {
		fmt.Println(v)
	}
}
```

我们并不需要打印索引，所以直接使用`_` 来接收它。执行结果如下：

![image-20200728165719760](img/image-20200728165719760.png)



## 多维数组

多维数组就是数组里面嵌套数组，这个在 python 中很容易实现，但是 GO 中定义稍微麻烦一点。

```go
package main

import "fmt"

func main() {
	var a1 = [3][3]string{
		{"广东", "深圳"},
		{"四川", "成都"},
		{"湖北", "武汉"},
	}
	fmt.Println(a1)
}
```

执行结果如下：

![image-20200728170410142](img/image-20200728170410142.png)



取值和遍历：

```go
package main

import "fmt"

func main() {
	var a1 = [3][3]string{
		{"广东", "深圳"},
		{"四川", "成都"},
		{"湖北", "武汉"},
	}
	fmt.Println(a1[1][1])

	// 遍历
	for _, v1 := range a1 {
		for _, v2 := range v1 {
			fmt.Println(v2)
		}
	}
}
```

执行结果如下：

![image-20200728170822007](img/image-20200728170822007.png)

> 注意，多维数组第一层可以使用推导 `...`，第二层就不行了。



## 补充说明

数组是值类型，赋值和传参都会复制整个数组，因此改变副本的值并不会影响原数组本身。

```go
package main

import "fmt"

func main() {
	var a1 = [3][3]string{
		{"广东", "深圳"},
		{"四川", "成都"},
		{"湖北", "武汉"},
	}
	// 修改数组本身
	a1[1][1] = "泸州"
	fmt.Println(a1)

	// 修改赋值的数组
	b1 := a1
	b1[1][1] = "绵阳"
	fmt.Println(a1)
	fmt.Println(b1)
}
```

执行结果如下：

![image-20200728172022508](img/image-20200728172022508.png)



## 练习

一、求数组 [1,3,5,7,8] 索引元素的和。

```go
package main

import "fmt"

func main() {
	var a1 = [5]int{1, 3, 5, 7, 8}
	sum := 0
	for _, v := range a1 {
		sum += v
	}
	fmt.Println(sum)
}
```



二、找出数组 [1,3,5,7,8] 两数之和等于 8 的索引。

```go
package main

import "fmt"

func main() {
	var a1 = [5]int{1, 3, 5, 7, 8}
	for x, _ := range a1 {
		for y := x + 1; y < len(a1); y++ {
			if a1[x]+a1[y] == 8 {
				fmt.Printf("(%d, %d)\n", x, y)
			}
		}
	}
}

```

