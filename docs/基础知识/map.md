## 说明

GO 语言提供了一种映射关系容器 `map`。是一种无序的 `kv` 数据结构，属于引用类型，必须初始化之后才能使用。



## 定义

结合 `make` 函数来在初始化的时候分配内存：

```
make(map[string]int, 8)
```

该方法的意义在于，定义一个 `map`，键的类型为 `string`，值的类型为 `int`，容量为 `8`。

```go
package main

import "fmt"

func main() {
	m1 := make(map[string]int, 8)
	m1["zhangsan"] = 18
	m1["lisi"] = 27
	m1["wangwu"] = 35

	fmt.Println(m1)
	fmt.Println(m1["lisi"])
	fmt.Printf("%T", m1)
}
```

执行结果如下：

![image-20200730114853812](img/image-20200730114853812.png)

可以发现内部确实是无序的，当然，也可以在声明的时候赋值：

```go
package main

import "fmt"

func main() {
	m1 := map[string]int{
		"zhangsan": 90,
		"lisi":     100,
	}
	fmt.Println(m1)
}
```



## 判断

```go
package main

import "fmt"

func main() {
	m1 := map[string]string {
		"username": "dylan",
		"password": "123456",
	}

	v, isOK := m1["user"]
	if isOK {
		fmt.Println(v)
	} else {
		fmt.Println(v)
		fmt.Println("键不存在")
	}
}
```

执行结果如下：

![image-20200730115635740](img/image-20200730115635740.png)

可以发现，我们使用两个变量来接收 `map` 取值，第一个是值，第二个是是否存在，如果不存在，则值为默认，如字符串是 `''`，`int` 是 `0`。



## 遍历

使用 `range` 遍历：

```go
package main

import "fmt"

func main() {
	m1 := map[string]string{
		"username": "dylan",
		"password": "123456",
	}

	for k, v := range m1 {
		fmt.Printf("%s-%s\n", k, v)
	}
}
```

执行结果如下：

![image-20200730120035573](img/image-20200730120035573.png)

如果只有一个变量接收，那么就是遍历 `key`。且 `map` 是无序的。



## 指定顺序遍历

```go
package main

import (
	"fmt"
	"math/rand"
	"sort"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano()) // 初始化随机

	// 生成随机的 map
	var m1 = make(map[string]int, 100)
	for i := 1; i <= 10; i++ {
		key := fmt.Sprintf("STU%02d", i)
		value := rand.Intn(100)
		m1[key] = value
	}
	fmt.Println(m1)

	// 将 map 的键取出放入切片中
	var s1 = make([]string, 0, 100)
	for key := range m1 {
		s1 = append(s1, key)
	}

	// 对切片中的键排序然后顺序输出
	sort.Strings(s1)
	for _, v := range s1 {
		fmt.Println(v, m1[v])
	}
}
```

执行结果如下：

![image-20200730140401961](img/image-20200730140401961.png)



## 删除

使用内置函数 `delete` 删除键值：

```go
package main

import "fmt"

func main() {
	m1 := map[string]string{
		"username": "dylan",
		"password": "123456",
	}

	delete(m1, "username")
	fmt.Println(m1)
}
```

执行结果如下：

![image-20200730134630311](img/image-20200730134630311.png)



## 值为 map 的切片

```go
package main

import "fmt"

func main() {
	var s1 = make([]map[string]string, 3, 10)
	for _, v := range s1 {
		fmt.Println(v)
	}
	// 初始化
	s1[0] = make(map[string]string, 10)
	s1[0]["username"] = "zhangsan"
	s1[0]["password"] = "123456"
	fmt.Println(s1)
}
```

值得注意的是，由于元素是 `map`，一开始是不能直接初始化的，所以需要专门先初始化才能添加值。



## 值为切片的 map

```go
package main

import "fmt"

func main() {
	var m1 = make(map[string][]string, 10)
	fmt.Println(m1)
	// 添加值
	m1["中国"] = append(m1["中国"], "北京", "上海")
	m1["日本"] = append(m1["日本"], "东京", "奈良")
	fmt.Println(m1)
}
```

切片和 `map` 有点不一样，切片不用再单独初始化也可以直接添加。



## 练习

一、写一个代码判断一个字符串中每个字母出现的次数。

```go
package main

import "fmt"

func main() {
	str := "ASNDIASDBAIDABDAIOSUA"
	var m1 = make(map[string]int, 26)
	for _, v := range str {
		key := fmt.Sprintf("%c", v)
		value, isExist := m1[key]
		if isExist {
			value += 1
			m1[key] = value
		}else {
			m1[key] = 1
		}
	}

	fmt.Println(m1)
}
```

执行结果如下：

![image-20200730145932917](img/image-20200730145932917.png)

