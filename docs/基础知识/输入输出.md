## 说明

在 GO 语言学习过程中用的最早最频繁的包：`fmt`



## Print

常用的输出方式，主要包含三种：

* `Print`：直接输出内容
* `Printf`：格式化输出内容
* `Println`：在结尾输出换行符

示例代码：

```go
package main

import "fmt"

func main() {
	fmt.Print("这是Print")
	fmt.Printf("这是%s\n", "Printf")
	fmt.Println("这是Println")
}
```

输出结果如下：

![image-20200803154521397](img/image-20200803154521397.png)



## Sprint

主要用于格式化拼接字符串：`Sprintf`

```go
package main

import "fmt"

func main() {
	name := "Dylan"
	age := 18
	content := fmt.Sprintf("My name is %s, i'm %d years old!", name, age)
	fmt.Println(content)
}
```

执行结果如下：

![image-20200804102409343](img/image-20200804102409343.png)



## 格式化占位符

| 占位符 | 说明                               |
| :----: | :--------------------------------- |
| %v   | 输出值                             |
| %+v  | 类似%v，但输出结构体时会添加字段名 |
| %#v  | 值的Go语法表示                     |
| %T   | 打印值的类型                       |
| %%   | 百分号                             |
| %b   | 二进制                                                 |
| %c   | 该值对应的unicode码值                                        |
| %d   | 十进制                                                       |
| %o   | 八进制                                                 |
| %x   | 十六进制，使用a-f                                      |
| %X   | 十六进制，使用A-F                                      |
| %U   | 表示为Unicode格式：U+1234，等价于”U+%04X”                    |
| %q   | 该值对应的单引号括起来的go语法字符字面值，必要时会采用安全的转义表示 |
| %t   | true 或 false |
| %e   | 科学计数法，如-1234.456e+78                            |
| %E   | 科学计数法，如-1234.456E+78                            |
| %g   | 根据实际情况采用%e或%f格式（以获得更简洁、准确的输出） |
| %G   | 根据实际情况采用%E或%F格式（以获得更简洁、准确的输出） |
| %s   | 直接输出字符串或者[]byte                                     |
| %p   | 表示为十六进制，并加上前导的0x |
| %f    | 默认宽度，默认精度 |
| %9f   | 宽度9，默认精度    |
| %.2f  | 默认宽度，精度2    |
| %9.2f | 宽度9，精度2       |
| %9.f  | 宽度9，精度0       |

示例如下：

```go
package main

import "fmt"

func main() {
	fmt.Printf("%v\n", 100)
	fmt.Printf("%+v\n", 100)
	fmt.Printf("%#v\n", 100)
	fmt.Printf("%T\n", 100)
	fmt.Printf("%d%%\n", 100)
	n := 65
	fmt.Printf("字符：%c\n", n)
	fmt.Printf("二进制：%b\n", n)
	fmt.Printf("十进制：%d\n", n)
	fmt.Printf("八进制：%o\n", n)
	fmt.Printf("十六进制：%x\n", n)
	m := 12.34
	fmt.Printf("%f\n", m)
	fmt.Printf("%12f\n", m)
	fmt.Printf("%.1f\n", m)
	fmt.Printf("%12.1f\n", m)
	fmt.Printf("%12.f\n", m)
}
```

执行结果如下：

![image-20200804104232262](img/image-20200804104232262.png)


