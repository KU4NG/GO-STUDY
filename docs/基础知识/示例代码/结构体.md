## 结构体

```go
package main

import "fmt"

func main() {
	// 自定义类型
	type newInt int
	var a1 newInt
	fmt.Printf("%T\n", a1) // main.myInt

	// 类型别名
	type myInt = int
	var a2 myInt
	fmt.Printf("%T\n", a2) // int
	// 类型别名更像另起一个好记的名字，但是实质话说它
	// 自定义类型则是一个全新的类型
	// 系统自带的类型别名：byte（uint8）和 rune（int32）

	// 结构体
	type userInfo struct {
		name  string
		age   int
		hobby []string
	}
	// GO 语言通过结构体来实现面向对象，这意味着结构体也能实现继承

	// 结构体相当于 python 的 class，需要实例化成具体的对象
	var user userInfo
	user.name = "Dylan"
	user.age = 18
	user.hobby = append(user.hobby, "篮球", "足球", "双色球")
	fmt.Printf("%#v\n", user) // main.userInfo{name:"Dylan", age:18, hobby:[]string{"篮球", "足球", "双色球"}}
	// 通过 #v 可以看到类型

	// 匿名结构体
	// 某些结构体可能只是执行到这里的时候才会用到，没有必要单独整个变量来存它，于是就有了匿名结构体
	var b1 struct {
		name string
		age  int
	}
	b1.name = "Dylan"
	b1.age = 18
	fmt.Println(b1) // {Dylan 18}
	// 直接定义的变量的类型就是结构体

	// 指针类型结构体
	// 使用 new 函数初始化，就能得到指针类型结构体
	var user2 = new(userInfo)
	fmt.Printf("%T\n", user2)  // *main.userInfo
	fmt.Printf("%#v\n", user2) // &main.userInfo{name:"", age:0, hobby:[]string(nil)}

	// 对于指针类型的结构体，GO语言提供了语法糖，所以依然能够和普通的结构体一样使用 . 来访问里面的属性
	user2.name = "Hello"
	// 按理说应该这样先取该指针的值，然后重新赋值，但是语法糖能够让我们不要这么麻烦
	(*user2).age = 18
	fmt.Println(user2) // &{Hello 18 []}

	// 通过 & 对结构体进行取值来实例化结构体，相当于 new
	var user3 = &userInfo{}
	fmt.Printf("%T\n", user3) // *main.userInfo

	// 初始化
	type c struct {
		name string
		age  int
	}

	// 键值初始化
	c1 := c{
		name: "Dylan",
		age:  18,
	}
	fmt.Println(c1) // {Dylan 18}

	// 结构体指针初始化
	c2 := &c{
		name: "Dylan",
		age:  18,
	}
	fmt.Println(c2) // &{Dylan 18}

	// 值列表初始化，必须按照顺序
	c3 := c{
		"Dylan",
		18,
	}
	fmt.Println(c3) // {Dylan 18}
	// 空结构体不占用内存空间

	// 练习
	type student struct {
		name string
		age  int
	}

	m := make(map[string]*student)

	// 初始化一个值为结构体类型的学生切片
	var stus []student
	stus = make([]student, 0)

	// 生成两个学生的实例
	s1 := student{
		name: "张三",
		age:  15,
	}
	s2 := student{
		name: "李四",
		age:  20,
	}

	// 将结构体加入切片中
	stus = append(stus, s1, s2)
	fmt.Println(stus) // [{张三 15} {李四 20}]

	// 以上代码可以可以简写
	stus1 := []student{
		{name: "张三", age: 15},
		{name: "李四", age: 20},
	}
	fmt.Println(stus1) // [{张三 15} {李四 20}]

	for _, ss := range stus1 {
		m[ss.name] = &ss
	}
	fmt.Println(m) // map[张三:0xc000004640 李四:0xc000004640]
	// 这里发现两个值的内存地址是一样的，这意味着它们修改的是同一个

	for k, v := range m {
		fmt.Println(k, v.name) // 李四 李四 // 张三 李四
	}

	// 构造函数调用
	d1 := newD("张三", 18)
	fmt.Printf("%#v\n", d1) // &main.d{name:"张三", age:18}
	// 构造函数命名一般使用结构体名称前面加个 new

	// 方法和接收者
	d1.printInfo() // 张三 18
	// 相当于给结构体定义了方法，方法在实例化之后可以直接调用
	// 方法内部可以直接调用结构体中的变量
	d1.setAgeNormal(30)
	fmt.Println("值类型接收者外部打印：", d1.age) // 值类型接收者外部打印： 18

	d1.setAgePos(40)
	fmt.Println("指针类型接收者外部打印：", d1.age) // 指针类型接收者外部打印： 40
	// 可以看到指针类型的接收者才能真正改变实例的值，值类型的接收者是复制了一份

	// 用户自定义类型设置方法
	var e1 userInt
	e1.printInfo() // Hello

	// 结构体的匿名字段
	type f1 struct {
		string
		int
	}

	var f2 = f1{
		"张三",
		18,
	}
	fmt.Println(f2.string) // 张三
	// 匿名结构体内部的同种类型只能出现一次，且传值需要按照顺序，调用直接点类型名称

	// 嵌套结构体
	type address struct {
		province string
		city     string
	}

	type worker struct {
		name    string
		age     int
		address address
	}

	g1 := worker{
		name: "张三",
		age:  18,
		address: address{
			province: "广东",
			city:     "深圳",
		},
	}

	fmt.Println(g1.name)             // 张三
	fmt.Println(g1.address.province) // 广东

	// 嵌套匿名字段
	type worker2 struct {
		name string
		age  int
		address
	}

	var g2 worker2
	g2.name = "李四"
	g2.province = "四川"
	g2.city = "成都"
	fmt.Println(g2.province) // 四川
	// 匿名嵌套如果外部没有相同的字段，可以直接调用内层的字段
	// 如果字段冲突，则需要指定具体的

	// 结构体继承
	h1 := dog{
		foot: 4,
		// 注意，传递的指针
		animal: &animal{
			name: "狗子",
		},
	}

	h1.eat()  // 吃吃吃
	h1.move() // 走走走
	// 相当于 dog 实例继承了 animal 的属性和方法

}

// 构造函数
type d struct {
	name string
	age  int
}

func newD(name string, age int) *d {
	return &d{
		name: name,
		age:  age,
	}
}

// 方法和接收者
func (d2 d) printInfo() {
	fmt.Println(d2.name)
	fmt.Println(d2.age)
}

// 方法前面相当于绑定结构体，一般用结构体名字的首字母小写来替代

// 值类型的接收者
func (d2 d) setAgeNormal(age int) {
	d2.age = age
	fmt.Println("值类型接收者内部打印：", d2.age) // 值类型接收者内部打印： 30
}

// 指针类型的接收者
func (d2 *d) setAgePos(age int) {
	d2.age = age
	fmt.Println("指针类型接收者内部打印：", d2.age) // 指针类型接收者内部打印： 40
}

// 任意类型都可以添加方法
type userInt int

func (u userInt) printInfo() {
	fmt.Printf("Hello")
}

// 结构体继承
type animal struct {
	name string
}

func (a *animal) move() {
	fmt.Println("走走走")
}

type dog struct {
	foot int
	*animal
}

func (d *dog) eat() {
	fmt.Println("吃吃吃")
}
```

