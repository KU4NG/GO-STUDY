## map

```go
package main

import "fmt"

func main() {
	// map 是一种无序的映射关系的 K/V 数据结构，属于引用类型，只有在初始化之后才能使用
	// 声明并赋值，当然一般不会这样使用
	var a1 = map[int]string{
		1: "张三",
		3: "李四",
	}

	fmt.Printf("%T\n", a1) // map[int]string
	fmt.Println(a1)        // map[1:张三 3:李四]

	// 声明，然后初始化，再赋值
	var a2 map[int]string
	a2 = make(map[int]string)
	fmt.Println(a2) // map[]
	a2[1] = "张三"
	a2[3] = "李四"
	a2[5] = "王五"
	fmt.Println(a2)    // map[1:张三 3:李四 5:王五]
	fmt.Println(a2[2]) // 零值
	fmt.Println(a2[3]) // 李四

	// 判断键是否存在，通过键取值其实是会返回两个值，一个是值，一个是是否存在
	_, isExist := a2[4]
	if isExist {
		fmt.Println("键存在")
	} else {
		fmt.Println("键不存在") // 键不存在
	}

	// 遍历 map
	a3 := map[string]string{
		"username": "Dylan",
		"password": "123456",
		"gender":   "male",
	}

	for k, v := range a3 {
		fmt.Printf("%s-%s\n", k, v) // password-123456 gender-male username-Dylan
	}

	// 删除
	a4 := map[int]string{
		1: "haha",
		2: "xixi",
	}
	delete(a4, 1)
	fmt.Println(a4) // map[2:xixi]

	// 值为 map 的切片定义和初始化方式
	// 切片 make 初始化需要指定长度和容量
	var b1 []map[string]string
	b1 = make([]map[string]string, 3, 10)
	fmt.Println(b1) // [map[] map[] map[]]

	var b2 = make([]map[string]string, 3, 10)
	fmt.Println(b2) // [map[] map[] map[]]

	// 遍历取出 map
	for _, v := range b1 {
		fmt.Println(v) // map[]
	}

	// 值得注意的是，一开始 make 初始化的是外层的切片
	// 内层的 map 是没有初始化的，所以在使用的时候需要重新初始化
	var b3 = make([]map[string]string, 3, 10)
	b3[0] = make(map[string]string)
	b3[0]["username"] = "Dylan"
	b3[0]["password"] = "123456"
	fmt.Println(b3) // [map[password:123456 username:Dylan] map[] map[]]

	// 值为切片的 map，map 和切片不同，内层不需要初始化
	var c1 map[string][]string
	c1 = make(map[string][]string) //map[]
	fmt.Println(c1)

	var c2 = make(map[string][]string) // map[]
	fmt.Println(c2)

	var c3 map[string][]string
	c3 = make(map[string][]string)
	c3["province"] = append(c3["province"], "广东", "四川", "贵州")
	fmt.Println(c3) // map[province:[广东 四川 贵州]]
	// 由于底层数组是同一个，所以这样赋值之后改变该切片的值 map 的值也会生效
	aa := c3["city"]
	c3["city"] = append(aa, "深圳", "广州", "成都", "泸州", "贵阳")
	fmt.Println(c3) // map[city:[深圳 广州 成都 泸州 贵阳] province:[广东 四川 贵州]]

	// 统计一个字符串中各个字母出现的次数
	str := "HANAISJDBHAIUDHBAWIDUBANDAIUDHASDUHASCXBANA"
	var tmpMap = make(map[string]int64)
	for _, x := range str {
		char := fmt.Sprintf("%c", x)
		tmpMap[char] += 1
	}
	fmt.Println(tmpMap) // map[A:10 B:4 C:1 D:6 H:5 I:4 J:1 N:3 S:3 U:4 W:1 X:1]
}
```

