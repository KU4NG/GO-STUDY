## 打印日志

GO 语言内置了 `log` 库用于实现简单的日志服务，可以简单的使用：

```go
package main

import "log"

func main() {
    log.Println("这是一条普通的日志")
    log.Fatal("只是一条 Fatal 日志")
    log.Panicln("这是一条 Panic 日志")
}
```

执行结果如下：

![image-20200812152008444](img/image-20200812152008444.png)

注意：

* 打印的日志会自动添加时间，Fatal 系列会在写入日志后调用 `os.Exit(1)` 退出。Panic 系列会在写入日志后 panic。



## 配置输出信息

默认的输出只有内容和时间，`log` 库提供了设置其它显示。以下时支持的 flags：

```go
const (
	Ldate         = 1 << iota     // the date in the local time zone: 2009/01/23
	Ltime                         // the time in the local time zone: 01:23:23
	Lmicroseconds                 // microsecond resolution: 01:23:23.123123.  assumes Ltime.
	Llongfile                     // full file name and line number: /a/b/c/d.go:23
	Lshortfile                    // final file name element and line number: d.go:23. overrides Llongfile
	LUTC                          // if Ldate or Ltime is set, use UTC rather than the local time zone
	Lmsgprefix                    // move the "prefix" from the beginning of the line to before the message
	LstdFlags     = Ldate | Ltime // initial values for the standard logger
)
```

可以通过设置查看：

```go
package main

import "log"

func main() {
    log.SetFlags(log.Ldate | log.Lmicroseconds | log.Lshortfile)
    log.Println("这是一条普通的日志")
}
```

执行结果如下：

![image-20200812153326938](img/image-20200812153326938.png)



## 配置日志前缀

可以在日志前面加上前缀用于区分不同的项目：

```go
package main

import "log"

func main()  {
    log.SetFlags(log.Ldate | log.Lmicroseconds | log.Lshortfile)
    log.SetPrefix("[Log Demo System] ")
    log.Println("这是一条简单的日志")
}
```

执行结果如下：

![image-20200812153802951](img/image-20200812153802951.png)



## 配置日志输出位置

我们需要把日志打印到文件而不是一直输出在控制台。

```go
package main

import (
    "fmt"
    "log"
    "os"
)

func main() {
    log.SetFlags(log.Ldate | log.Lmicroseconds | log.Lshortfile)
    log.SetPrefix("[Log demo] ")

    // 打开文件
    file, err := os.OpenFile("./test.log", os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
    if err != nil {
        fmt.Println("打开日志文件失败！")
        return
    }
    defer file.Close()
    // 写入日志
    log.SetOutput(file)
    log.Println("这是测试写入文件的日志")

}
```

查看文件：

![image-20200812154525838](img/image-20200812154525838.png)



## 使用 new 创建

```go
package main

import (
    "log"
    "os"
)

func main() {
    logger := log.New(os.Stdout, "[Log demo]", log.Ldate|log.Lmicroseconds|log.Lshortfile)
    logger.Println("这是测试日志")
}
```

执行结果如下：

![image-20200812154908236](img/image-20200812154908236.png)

通过上面的可以发现相同的 log 库功能不完善，只能实现简单的需求，更复杂的可以使用 `logrus` 这些库。

