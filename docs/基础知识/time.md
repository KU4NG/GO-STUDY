## 时间类型

时间的类型为 `time.Time`，可以通过下面的方法来获取：

```go
package main

import (
    "fmt"
    "time"
)

func main() {
    now := time.Now()
    fmt.Printf("TYPE: %T\n", now)
    fmt.Printf("VALUE: %v\n", now)

    // 获取具体的时间
    fmt.Println(now.Year())
    fmt.Println(now.Month())
    fmt.Println(now.Day())
    fmt.Println(now.Hour())
    fmt.Println(now.Minute())
    fmt.Println(now.Second())
    fmt.Println(now.Date())
}
```

执行结果如下所示：

![image-20200812140829850](img/image-20200812140829850.png)



## 时间戳

时间戳（timestamp）是从 `1970年1月1日` 至今的总毫秒数，也被称为 Unix 时间戳。

```go
package main

import (
    "fmt"
    "time"
)

func main() {
    now := time.Now()
    ts1 := now.Unix() // 时间戳
    fmt.Println(ts1)
    ts2 := now.UnixNano() // 纳秒时间戳
    fmt.Println(ts2)

    // 将时间戳转换成时间
    a := 1597212743
    timeObj := time.Unix(int64(a), 0)
    fmt.Println(timeObj.Year())
    fmt.Println(timeObj.Date())
}
```

执行结果如下：

![image-20200812141721022](img/image-20200812141721022.png)



## 时间间隔

两个时间之间间隔的间隔，`time.Duration` 表示一毫秒，也可以使用 `time.Second` 表示一秒。

```go
package main

import (
    "fmt"
    "time"
)

func main() {
    fmt.Println("开始")
    time.Sleep(10 * time.Second)
    fmt.Println("结束")
}
```

执行结果如下：

![image-20200812142255182](img/image-20200812142255182.png)

使用 `time.Sleep()` 来休眠。



## 时间操作

时间相加：`Add`，计算一个时间加上时间间隔。

```go
package main

import (
    "fmt"
    "time"
)

func main() {
    now := time.Now()
    later := now.Add(time.Hour * 10)
    fmt.Println(now)
    fmt.Println(later)
}
```

执行结果如下：

![image-20200812142824356](img/image-20200812142824356.png)



时间差：`Sub`，计算两个时间的差

```go
package main

import (
    "fmt"
    "time"
)

func main() {
    now := time.Now()
    later := now.Add(time.Hour * 10)
    sub := later.Sub(now)
    fmt.Println(sub)
}
```

执行结果如下：

![image-20200812143223059](img/image-20200812143223059.png)



判断两个时间是否相同：`Equal`，和 == 不同，该方法会对比时区

判断两个时间的先后：`Before` / `After`

```go
package main

import (
    "fmt"
    "time"
)

func main() {
    now := time.Now()
    later := now.Add(10 * time.Minute)

    // 判断时间是否相同
    isEqual := later.Equal(now)
    fmt.Println(isEqual)

    // 判断时间先后
    isBefore := later.Before(now)
    fmt.Println(isBefore)
    isAfter := later.After(now)
    fmt.Println(isAfter)
}
```

执行结果如下：

![image-20200812143846694](img/image-20200812143846694.png)



## 定时器

使用 `time.Tick()` 来设置定时器。

```go
package main

import (
    "fmt"
    "time"
)

func main() {
    t1 := time.Tick(time.Second)
    for i := range t1 {
        fmt.Println(i)
    }
}
```

执行结果如下：

![image-20200812145004525](img/image-20200812145004525.png)



## 时间格式化

GO 语言中的时间格式化不像其它的 Y-M-D，而是使用 GO 语言发布的时间 2006年1月2日15时4分（口诀：2006 1 2 3 4 5）

```go
package main

import (
    "fmt"
    "time"
)

func main() {
    now := time.Now()
    // 二十四小时制
    fmt.Println(now.Format("2006-01-02 15:04:05.000"))
    // 十二小时制
    fmt.Println(now.Format("2006/01/02 03:04:05.000 PM"))
}
```

执行结果如下：

![image-20200812145817597](img/image-20200812145817597.png)



## 解析字符串时间

```go
package main

import (
    "fmt"
    "time"
)

func main() {
    loc, _ := time.LoadLocation("Asia/Shanghai")
    t1 := "2020-07-07 14:13:55"
    timeObj, _ := time.ParseInLocation("2006-01-02 15:04:05", t1, loc)
    fmt.Println(timeObj)
}
```

执行结果如下：

![image-20200812150559265](img/image-20200812150559265.png)



