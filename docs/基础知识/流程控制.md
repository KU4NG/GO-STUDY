## 说明

GO 语言中常见的流程控制语句包含：`if`， `for`，`switch` ，`goto` 

其目的是为了简化代码，提高代码的复用性。



## if else（分支结构）

条件分支，示例代码：

```go
package main

import "fmt"

func main() {
	age := 20
	if age < 18 {
		fmt.Println("未成年")
	} else if age <= 35 {
		fmt.Println("青年")
	} else if age <= 60 {
		fmt.Println("中年")
	} else {
		fmt.Println("老年")
	}
}
```

值得注意的是：

* `else` 语句必须和前一个 `if` 的 `}` 在同一行，否则会被识别成单独的语句而报错。
* 如果 `if` 条件中包含了变量的赋值，那么改变了只在该 `if` 中生效，如：`if a:=10;a >20 {}` 这种。



## for（循环结构）

常见的 `for` 循环定义方式：

```go
package main

import "fmt"

func main() {
	for i := 0; i < 10; i++ {
		fmt.Println(i)
	}
}
```

条件为：初始值，条件表达式，结束语句



也可以把初始值定义在外部，这样该值就不是局部变量，但是条件前面还是需要 `;`。

```go
package main

import "fmt"

func main() {
	i := 0
	for ; i < 5; i++ {
		fmt.Println(i)
	}
	fmt.Println("i的最终值：", i)
}
```

执行结果如下：

![image-20200728143558364](img/image-20200728143558364.png)



甚至可以进行最细的拆分，只需要条件：

```go
package main

import "fmt"

func main() {
	i := 0
	for i < 5 {
		fmt.Println(i)
		i++
	}
}
```



最后就是死循环，不加任何条件判断：

```GO
package main

import "fmt"

func main() {
	for {
		fmt.Println("Hello GO")
	}
}
```



还有就是 `for range` 遍历：

```go
package main

import "fmt"

func main() {
	str := "Hello"
	for k, v := range str {
		fmt.Printf("Key is %d，Vaule is %v\n", k, v)
	}
}
```

执行结果如下，值是十六进制的值：

![image-20200728144721823](img/image-20200728144721823.png)



## switch case

多条件选择：

```go
package main

import "fmt"

func main() {
	num := 1
	switch num {
	case 1:
		fmt.Println("大拇指")
	case 2:
		fmt.Println("食指")
	case 3:
		fmt.Println("中指")
	case 4:
		fmt.Println("无名指")
	case 5:
		fmt.Println("小拇指")
	default:
		fmt.Println("输入错误")
	}
}
```

在 `switch` 多值判断中，执行顺序为从上到下，`default` 为非必须。

当然，如果有多个值也是可以的，只需要值之间使用逗号隔开：

```go
package main

import "fmt"

func main() {
	num := 1
	switch num {
	case 1,2,3:
		fmt.Println("haha")
	case 4,5,6:
		fmt.Println("xixi")
	default:
		fmt.Println("hehe")
	}
}
```

也可以加入条件判断来处理：

```go
package main

import "fmt"

func main() {
	num := 10
	switch {
	case num < 10:
		fmt.Println("个位数")
	case num < 100:
		fmt.Println("十位数")
	default:
		fmt.Println("hehe")
	}
}
```

当满足一个条件之后会自动跳出整个 `switch` 语句。



## goto（标签跳转）

跳转到指定标签，常用于多层循环的时候跳出循环。

```go
package main

import "fmt"

func main() {
	for x := 0; x < 10; x++ {
		for y := 0; y < 10; y++ {
			if y == 5 {
				goto breakTag
			}
			fmt.Println(x, y)
		}
	}

breakTag: // 定义标签
	fmt.Println("跳出循环啦")
}
```

执行结果如图所示：

![image-20200728152439632](img/image-20200728152439632.png)



## break（跳出循环）

break 可以在 `for`，`switch`，`select` 中使用，用于跳出循环。

```go
package main

import "fmt"

func main() {
	for x := 0; x < 10; x++ {
		if x == 5 {
			break
		}
		fmt.Println(x)
	}
	fmt.Println("结束啦")
}
```

执行结果如下：

![image-20200728153642213](img/image-20200728153642213.png)

当然，break 跳出的只能是当前这层循环，如果想跳出多层，则每层都需要。



## continue（跳过本次循环）

通过判断条件，可以对本次循环进行跳过，进入下一次循环：

```go
package main

import "fmt"

func main() {
	for x := 0; x < 5; x++ {
		if x == 2 {
			continue
		}
		fmt.Println(x)
	}
	fmt.Println("结束啦")
}
```

执行结果如下所示：

![image-20200728154013023](img/image-20200728154013023.png)

可以发现当 x 等于 2 的时候直接就跳过了。



## 九九乘法表

```go
package main

import "fmt"

func main() {
	for x := 1; x < 10; x++ {
		for y := 1; y <= x; y++ {
			fmt.Printf("%d * %d = %d  ", y, x, x*y)
		}
		fmt.Printf("\n")
	}
}
```

输出结果如下：

![image-20200728154705315](img/image-20200728154705315.png)

