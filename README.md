#### 使用方法
* 安装 NODEJS
下载地址：
>http://nodejs.cn/download/
* 安装 cnpm
```
npm install -g cnpm
```
* 安装 docsify
```
cnpm install -g docsify
```
文档地址：
> https://docsify.js.org/#/zh-cn/more-pages
* 启动服务
```
docsify serve GO-STUDY
```